---
layout: post
title: Project Euler – Solution to problem 2
date: 2018-02-24 18:04 +0100
categories:
  - Programming
  - Project Euler
tags:
  - C#
  - Code
comments: true
---

## Introduction

I decided, a few hours ago, to solve some problems from [Project Euler](https://projecteuler.net/about) and I've written a small post about the first (very easy) problem. This evening I solved the second one, which was nothing like difficult, but I've found some small interesting implementation details.

## The problem

[Problem #2](https://projecteuler.net/problem=2) asks for the sum of all even-valued terms below 4 millions of the Fibonacci sequence.

## The solution

There is maybe a very smart mathematical solution to this problem, but I don't know it 😃 What I've done is take all terms below 4 000 000, keep the even ones and sum.

I've first written a function that can generate an arbitraty long Fibonacci sequence:

```csharp
public static IEnumerable<BigInteger> Fibonacci()
{
    yield return 1;
    yield return 1;

    BigInteger a = 1;
    BigInteger b = 1;
    while (true)
    {
        BigInteger sum = a + b;
        yield return sum;
        a = b;
        b = sum;
    }
}
```

This function takes advantage of:

* The `BigInteger` class (from `System.Numerics`) to avoid integer overflow
* The `yield` keyword to generate an `IEnumerable`. Then, for example, I can get the first 10 terms by doing `Fibonacci().Take(10)`

Then the rest is pretty straightforward, using LINQ:

```csharp
public class Problem0002
{
    public BigInteger Solve()
    {
        return Functions.Fibonacci()
            .TakeWhile(i => i <= 4_000_000)
            .Where(i => i % 2 == 0)
            .Aggregate(BigInteger.Add);
    }
}
```

Et voilà! 🎉
