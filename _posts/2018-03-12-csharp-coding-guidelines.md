---
layout: post
title: C# coding guidelines
date: 2018-03-12 17:21 +0100
categories:
  - Programming
  - Guidelines
tags:
  - C#
  - Guidelines
comments: true
---

## Introduction

I recently had a debate with some of my colleagues on whether "coding rules" were a good ideas for a company and/or for personal projects.

To give some context, I work in a company where about 150 developers work on the same C# code base.

To me it appears that having some "soft rules" can be a good idea, to avoid mixing-up very different styles that can limit readability and prevent easy mistakes. However I have seen too many coding rules that are either, in my opinion, to shallow or too complex. To me, coding rules should only be about the language and not the application design ("Follow SOLID principles" should not be a coding rule, it's just part of any developer's job).

I've decided to write down my own rules, the one I followed for my personal projects for years without having formally stated them. Well, they are *coding guidelines* rather than *coding rules*.

## My personal C# coding guidelines

**Table of contents:**
- [Code style](#code-style)
    - [Formatting](#formatting)
    - [Structure](#structure)
    - [Naming conventions](#naming-conventions)
    - [Readability](#readability)
    - [Documentation](#documentation)
- [Best practices](#best-practices)

### Code style

#### Formatting

- Use **4 spaces** for indentation. Tabs are prohibited.

- Use **LF** line breaks. (Configure git as "Checkout Windows-style, commit Unix-style" on Windows and "Checkout as-is, commit Unix-style" on Unix)

- Encoding is **UTF-8**.

- Use **Allman** indentation style.

    ```csharp
    // Do
    public class MyClass
    {
        private void DoSomething()
        {
            if (condition)
            {
                // ...
            }
            else
            {
                // ...
            }
        }
    }

    // Don't
    public class MyClass {
        private void DoSomething() {
            if (condition) {
                // ...
            } else {
                // ...
            }
        }
    }
    ```
- No consecutive blank lines.

- No consecutive whitespaces except for indentation.

- No trailing whitespaces.

#### Structure

- Fields can olny be *private* or *protected*.

- Using directives should be specified *outside* of `namespace` declarations and should be sorted alphabetically, with the exception of `System` directives that go first.

    ```csharp
    // Do
    using System.Linq;
    using System.Numerics;
    using Core.Lib;
    using Xunit;

    namespace Tests
    {
        public class TestClass
        {
        }
    }

    // Don't
    namespace Tests
    {
        using System.Linq;
        using System.Numerics;
        using Core.Lib;
        using Xunit;

        public class TestClass
        {
        }
    }

    // Don't
    using Core.Lib;
    using System.Linq;
    using System.Numerics;
    using Xunit;

    namespace Tests
    {
        public class TestClass
        {
        }
    }
    ```

- Always specify the visibility, even if it's the default.

    ```csharp
    // Do
    internal class Client
    {
        private int _count;
    }

    // Don't
    class Client
    {
        int _count;
    }
    ```

- Class elements must be separated by blank lines.

    ```csharp
    // Do
    public int Count1 { get; }

    public int Count2 { get; }

    // Don't
    public int Count1 { get; }
    public int Count2 { get; }
    ```

- Arrange class members on the following order:
  - Fields
  - Constructors
  - Delegates
  - Events
  - Properties
  - Public methods
  - Private methods

#### Naming conventions

- Name constants using `UPPER_SNAKE_CASE`.

- Name fields using `_camelCase`.

- Name variables and parameters using `camelCase`. No Hungarian notation.

    ```csharp
    // Do
    string name = "Gandalf";
    private void DoSomething(string firstName);

    // Don't
    string strName = "Gandalf";
    private void DoSomething(string FirstName);
    ```

- Name properties, classes, enums, methods... using `PascalCase`.

- Prefix interface names with "I". Don't prefix other types.

    ```csharp
    // Do
    public interface IEnumerable { }
    public class List { }

    // Don't
    public interface Enumerable { }
    public class CList { }
    ```

- Namespaces must be in `PascalCase.PascalCase`

    ```csharp
    // Do
    namespace SupernovaPackages.Core.Utils { }

    // Don't
    namespace Supernova_Packages.Core.Utils { }
    namespace supernovaPackages.core.utils { }
    ```

- Prefix events with "On"

    ```csharp
    // Do
    public static event CloseCallback OnClose;

    // Don't
    public static event CloseCallback Close;

    // Don't
    public static event CloseCallback Closing;
    ```

- Treat acronyms as words

    ```csharp
    // Do
    var siteUrl = "gitlab.com";
    var userId = 42;

    // Don't
    var siteURL = "gitlab.com";
    var userID = 42;
    ```

- Name `enums` with singular nouns except for bit fields (flags).

    ```csharp
    // Do
    public enum Color
    {
        Blue,
        White,
        Red
    }

    [Flags]
    public enum Colors
    {
        Blue = 1 << 0,
        White = 1 << 1,
        Red = 1 << 2
    }
    ```


#### Readability

- Avoid `this.` unless absolutely necessary.

- Avoid lines longer than **200 characters**.

- Use `var` only when the type is obvious.

- Define each type in their own file. The name of the file must match the name of the type.

- Only one declaration per line.

    ```csharp
    // Do
    string foo;
    string bar;

    // Don't
    string foo, bar;
    ```

- Only one statement per line.

    ```csharp
    // Do
    Foo();
    Bar();

    // Don't
    Foo(); Bar();
    ```
- Split long method chains into multiples lines. Only one call per line.

    ```csharp
    // Do
    string[] sharedBooktitles = users
        .SelectMany(user => user.Books)
        .Where(book => book.IsShared)
        .Select(book => book.Title)
        .ToArray();

    // Don't
    string[] sharedBooktitles = users.SelectMany(user => user.Books).Where(book => book.IsShared).Select(book => book.Title)).ToArray();

    // Don't
    string[] sharedBooktitles = users
        .SelectMany(user => user.Books)
        .Where(book => book.IsShared)
        .Select(book => book.Title).ToArray();
    ```

- Use language keywords instead of BCL types.

    ```csharp
    // Do
    int i = int.Parse("42");

    // Don't
    Int32 i = Int32.Parse("42");
    ```

- Don't use `#region`

- Avoid using unsigned types.

- Use object initializers.

    ```csharp
    // Do
    var player = new Player("Alice")
    {
        Age = 45,
        Score = 13568
    };

    // Don't
    var player = new Player("Alice");
    player.Age = 45;
    player.Score = 13568;
    ```

- Use early `return` statements

    ```csharp
    // Do
    public void Foo(int age)
    {
        if (age < 18) return;

        // Do something.
    }

    // Do
    public void Foo(int age)
    {
        if (age >= 18)
        {
            // Do something.
        }
    }
    ```

- Avoid using abbreviations (except common ones, like XML, URL, API...).

    ```csharp
    // Do
    int userExperience;
    string employeeGroup;

    // Don't
    int usrExp;
    string empGrp;
    ```

- Avoid names redundency (regarding to the context)

    ```csharp
    // Do
    public class User
    {
        public string Name { get; set; }
    }

    // Don't
    public class User
    {
        public string UserName { get; set; }
    }
    ```

#### Documentation

- Write comments on separete line.

    ```csharp
    // Do
    public int Foo()
    {
        // Returns the answer.
        return 42;
    }

    // Don't
    public int Foo()
    {
        return 42; // Returns the answer.
    }
    ```

- Comments are full sentences, beginning with an uppercase letter and ending with a period.

    ```csharp
    // Do
    public int Foo()
    {
        // Returns the answer.
        return 42;
    }

    // Don't
    public int Foo()
    {
        // returns the answer
        return 42;
    }

    // Don't
    public int Foo()
    {
        // The answer.
        return 42;
    }
    ```

- Insert a single whitespace after the comment delimiter

    ```csharp
    // Do
    public int Foo()
    {
        // Returns the answer.
        return 42;
    }

    // Don't
    public int Foo()
    {
        //Returns the answer.
        return 42;
    }
    ```

- Don't write blocks with slashes, asterisks or other characters.

    ```csharp
    // Do
    public int Foo()
    {
        // Returns the answer.
        return 42;
    }

    // Don't
    public int Foo()
    {
        // **********
        // Returns the answer.
        // **********
        return 42;
    }
    ```

### Best practices

- Always use UTC time.

    ```csharp
    // Do
    var dt = DateTime.UtcNow;

    // Don't
    var dt = DateTime.Now;
    ```

- Enclose conditional statements with braces or write them on a single line.

    ```csharp
    // Do
    if (condition) DoSomething();
    if (condition)
    {
        DoSomething();
    }

    // Don't
    if (condition)
        DoSomething();
    ```

- Use `nameof(...)` whenever possible.

    ```csharp
    // Do
    throw new ArgumentNullException(nameof(argument));

    // Don't
    throw new ArgumentNullException("argument");
    ```

- Use `StringBuilder` when performing a lot of concatenations.

- Use conditional operators `&&` and `||` instead of `&` and `|`

- Initialize bit fields (flag enums) explicitly. Use shifting notation to initialize values.

    ```csharp
    // Do
    [Flags]
    public enum Colors
    {
        Blue = 1 << 0,
        White = 1 << 1,
        Red = 1 << 2
    }

    // Don't
    [Flags]
    public enum Colors
    {
        Blue,
        White,
        Red
    }

    // Don't
    [Flags]
    public enum Colors
    {
        Blue = 1,
        White = 2,
        Red = 4
    }
    ```

- Don't specify type or values for non bit fields enums

    ```csharp
    // Do
    public enum Color
    {
        Blue,
        White,
        Red
    }

    // Don't
    public enum Color
    {
        Blue = 1,
        White = 2,
        Red = 3
    }

    // Don't
    public enum Color : long
    {
        Blue,
        White,
        Red
    }
    ```

- Don't hide inherited members with the `new` keyword

- Use `struct` instead of `class` only if **all** the following are true:
    - It represents a single value (much like `bool`, `int`...)
    - An instance weight under 16 bytes
    - It is immutable
    - It won't need frequent boxing
    
- Define, and define only, *protected* or *internal* constructors in abstract classes.

- Avoid using marker interfaces (empty interfaces)