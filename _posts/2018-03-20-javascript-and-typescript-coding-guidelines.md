---
layout: post
title: JavaScript and TypeScript coding guidelines
date: 2018-03-20 10:46 +0100
categories:
  - Programming
  - Guidelines
tags:
  - JavaScript
  - TypeScript
  - Guidelines
comments: true
---

## Introduction

Last week I wrote down my [C# coding guidelines]({{ site.baseurl }}{% post_url 2018-03-12-csharp-coding-guidelines %}). Since I'm about to start a new TypeScript project, I've decided to do the same for this language (most of it also applies to simple JavaScript).

There are already a lot of JavaScript coding rules out there, but I was disatisfied with them. For instance I disliked the common recommendation to use 2 spaces for indentation, I find 4 spaces much more readable. And those adressing TypeScript were a bit too shallow in my opinion.

Since I have less experience with TypeScript (and JavaScript) than I have with C#, the guidelines I wrote are based on what I understand now and may evolve in the future.

As before, I've tried to state the fundamental guidelines I want to follow for my personal projects.

## My personal JavaScript and TypeScript coding guidelines

**Table of contents:**
- [Code style](#code-style)
    - [Formatting](#formatting)
    - [Structure](#structure)
    - [Naming conventions](#naming-conventions)
    - [Readability](#readability)
    - [Documentation](#documentation)
- [Best practices](#best-practices)
    - [Types](#types)
    - [Organization](#organization)
    - [Avoiding stupid bugs](#avoiding-stupid-bugs)
    - [Clean code](#clean-code)

### Code style

#### Formatting

- Use **4 spaces** for indentation. Tabs are prohibited.

- Use **LF** line breaks. (Configure git as "Checkout Windows-style, commit Unix-style" on Windows and "Checkout as-is, commit Unix-style" on Unix)

- Encoding is **UTF-8**.

- Use **1TBS** indentation style.

    ```js
    // Do
    class MyClass {
        function doSomething() {
            if (condition) {
                // ...
            } else {
                // ...
            }
        }
    }

    // Don't
    class MyClass
    {
        function doSomething()
        {
            if (condition)
            {
                // ...
            }
            else
            {
                // ...
            }
        }
    }
    ```
- No consecutive blank lines.

- No consecutive whitespaces except for indentation.

- No trailing whitespaces.

- No space between function name and left parenthesis, single space between right parenthesis and curly brace

    ```ts
    // Do
    function foo() {
    }

    // Don't
    function foo(){
    }
    function foo () {
    }
    ```

- In type declaration, place no space before the colon and a single space after it

    ```ts
    // Do
    function foo(bar: string, baz: number) {
    }

    // Don't
    function foo(bar :string, baz:number) {
    }
    ```

#### Structure

- Use standard and minimal imports

    ```js
    // Do
    import { SomeClass, someFunction } from 'some-module'

    // Avoid
    import SomeModule from 'some-module';

    // Don't
    const SomeModule = require('some-module');
    ```

- Group imports from a module on the same instruction

    ```js
    // Do
    import { foo, bar } from 'baz';

    // Don't
    import { foo } from 'baz';
    import { bar } from 'baz';
    ```

- Write all imports at the top of the file

- Work on class rather than on prototype and constructor functions

    ```js
    // Do
    class Foo {
        constructor(value) {
            this.baz = value;
        }

        function bar() {
            return this.baz + 42;
        }
    }

    // Don't
    function Foo(value) {
        this.baz = value;
    }
    Foo.prototype.bar = function() {
        return this.baz + 42;
    };
    ```

#### Naming conventions

- Use `camelCase` for variable and class members names

    ```js
    // Do
    const fooBar = 42;
    let fooBar = 42;

    // Don't
    let FooBar = 42;
    let foobar = 42;
    ```

- Use `camelCase` for function and method names

    ```js
    // Do
    function fooBar() {
        // ...
    }

    // Don't
    function FooBar() {
        // ...
    }
    function foobar() {
        // ...
    }
    ```

- Use `PascalCase` for class, interface and enum names

    ```js
    // Do
    class FooBar {
    }

    // Don't
    class fooBar {
    }
    class foobar {
    }
    ```

- Prefix interface names with "I"

    ```ts
    // Do
    interface IFoo {
    }

    // Don't
    interface Foo {
    }
    ```

- Name `enums` with singular nouns except for bit fields

    ```ts
    // Do
    enum Color {
        Blue,
        White,
        Red
    }

    enum Colors {
        None = 0,
        Blue = 1 << 0,
        White = 1 << 1,
        Red = 1 << 2
    }

    // Don't
    enum Colors {
        Blue,
        White,
        Red
    }

    enum Color {
        None = 0,
        Blue = 1 << 0,
        White = 1 << 1,
        Red = 1 << 2
    }
    ```

- Use `PascalCase` for enum members

    ```ts
    // Do
    enum Color {
        Blue,
        White,
        Red
    }

    // Don't
    enum Color {
        blue,
        white,
        red
    }
    enum Color {
        BLUE,
        WHITE,
        RED
    }
    ```

- Use `kebab-case` for file names.

- Use type name for class or enum files.

- Use type name without prefix and followed by ".contract" for interface files (e.g. `IFoo` in `foo.contract.ts`)

- Treat acronyms as words.

    ```ts
    // Do
    const siteUrl = "gitlab.com";
    const userId = 42;

    // Don't
    const siteURL = "gitlab.com";
    const userID = 42;
    ```

#### Readability

- No empty contructor.

- Prefer `Foo[]` to `Array<Foo>` type annotation.

- Avoid lines longer than **200 characters**.

- Define each type in their own file.

- Only one declaration per line.

    ```ts
    // Do
    let foo = 1;
    let bar = 2;

    // Don't
    let foo = 1, bar = 2;
    ```

- Only one statement per line.

    ```ts
    // Do
    foo();
    bar();

    // Don't
    foo(); bar();
    ```

- Split long method chains into multiples lines. Only one call per line.

    ```ts
    // Do
    const foo = bar
        .filter(b => b.baz > 42)
        .map(b => b.qux)
        .reduce((a, b) => a.concat(b));

    // Don't
    const foo = bar.filter(b => b.baz > 42).map(b => b.qux).reduce((a, b) => a.concat(b));

    // Don't
    const foo = bar
        .filter(b => b.baz > 42)
        .map(b => b.qux).reduce((a, b) => a.concat(b));
    ```

- Use early return statements

    ```ts
    // Do
    function foo(age) {
        if (age < 18) return;

        // Do something.
    }

    // Don't
    function foo(age) {
        if (age >= 18) {
            // Do something.
        }
    }
    ```

- Always explicitly define return type

    ```ts
    // Do
    function foo(): number {
        return 42;
    }

    // Don't
    function foo() {
        return 42;
    }
    ```

- Prefer litteral initialization.

    ```ts
    // Do
    const foo = {};
    const bar = [];

    // Don't
    const foo = new Object();
    const bar = new Array();
    ```

- Use shorthands in object creation

    ```ts
    // Do
    const foo = {
        bar,
        baz(value) {
            return value + 42;
        }
    };

    // Don't
    const foo = {
        bar: bar,
        baz: function(value) {
            return value + 42;
        }
    }
    ```

- Don't quote properties, except for invalid identifiers

    ```ts
    // Do
    const foo = bar.baz;
    const foo = bar['baz-qux'];
    const foo = {
        'baz-qux': 42
    };

    // Don't
    const foo = bar['baz'];
    const foo = {
        'baz': 42
    };
    ```

#### Documentation

- Write comments on separate line.

    ```ts
    // Do
    function foo() {
        // Returns the answer.
        return 42;
    }

    // Don't
    function foo() {
        return 42; // Returns the answer.
    }
    ```

- Use [JSDoc](https://devhints.io/jsdoc) comments when useful

    ```ts
    /**
     * This is a function.
     *
     * @param {string} n - A string param
     * @return {string} A good string
     */
    function foo(n) {
        return n;
    }
    ```

- Comments are full sentences, beginning with an uppercase letter and ending with a period.

    ```ts
    // Do
    function foo() {
        // Returns the answer.
        return 42;
    }

    // Don't
    function foo() {
        // returns the answer
        return 42;
    }

    // Don't
    function foo() {
        // The answer.
        return 42;
    }
    ```

- Insert a single whitespace after the comment delimiter

    ```ts
    // Do
    function foo() {
        // Returns the answer.
        return 42;
    }

    // Don't
    function foo() {
        //Returns the answer.
        return 42;
    }
    ```

- Don't write blocks with slashes, asterisks or other characters.

    ```ts
    // Do
    function foo() {
        // Returns the answer.
        return 42;
    }

    // Don't
    function foo() {
        // **********
        // Returns the answer.
        // **********
        return 42;
    }
    ```

### Best practices

#### Types

- Don't write interfaces in `.d.ts` files (see <https://stackoverflow.com/a/42257742>).

- Use `number`, `boolean`, `string` and `object` rather than `Nmber`, `Boolean`, `String` and `Object` types

#### Organization

- Use [namespaces](https://www.typescriptlang.org/docs/handbook/namespaces.html) to organize complex code

    ```ts
    // Do
    namespace Foo {
        class Bar {

        }
    }

    // in another file
    import Bar = Foo.Bar
    ```

#### Avoiding stupid bugs

- Enclose conditional statements with braces or write them on a single line.

    ```ts
    // Do
    if (condition) doSomething();
    if (condition) {
        doSomething();
    }

    // Don't
    if (condition)
        doSomething();
    ```

- Don't use implied global variables.

- Never use the `eval` function.

- Never use the `Function` constructor.

- Initialize bit fields explicitly. Use shifting notation.

    ```ts
    // Do
    enum Colors {
        None = 0,
        Blue = 1 << 0,
        White = 1 << 1,
        Red = 1 << 2
    }

    // Don't
    enum Colors {
        None,
        Blue,
        White,
        Red
    }

    enum Colors {
        None = 0,
        Blue = 1,
        White = 2,
        Red = 4
    }
    ```

- Always use `===` and `!==` instead of `==` and `!=`

- Use default parameter instead of argument mutation

    ```ts
    // Do
    function foo(bar = {}) {
        // ...
    }

    // Don't
    function foo(bar) {
        bar = bar || {};
    }
    ```

#### Clean code

- Don't use `var`

    ```js
    // Do
    let foo = 42;

    // Don't
    var foo = 42;
    ```

- Use `const` whenever possible

    ```js
    // Do
    const foo = 42; // not changed later
    let foo = 42; // changed later
    ```

- Prefer using single quotes

- Use interpolation rather than concatenation

    ```ts
    // Do
    const foo = `Hello ${name}`;

    // Don't
    const foo = 'Hello ' + name;
    ```

- Always terminate statements with semicolon

    ```ts
    // Do
    function foo() {
        const bar = 42;
        return `Answer = ${bar}`;
    }

    // Don't
    function foo() {
        const bar = 42
        return `Answer = ${bar}`
    }
    ```

- Use spread operator when possible.

- Use destructuring.

    ```ts
    // Do
    function foo({ bar, baz }) {
        return `${bar} ${baz}`;
    }

    // Do
    const foo = [1, 2, 3, 4];
    const [bar, baz] = foo;

    // Don't
    function foo(obj) {
        return `${obj.bar} ${obj.baz}`;
    }

    // Don't
    const foo = [1, 2, 3, 4];
    const bar = foo[0];
    const baz = foo[1];
    ```

- Returns objects rather than arrays.

    ```ts
    // Do
    function foo() {
        // ...
        return { bar, baz, qux };
    }

    const { bar, qux } = foo();

    // Don't
    function foo() {
        // ...
        return [ bar, baz, qux ];
    }

    const [bar, _, qux] = foo();
    ```

- Use the rest parameter syntax instead of `arguments`

    ```ts
    // Do
    function foo(bar, ...baz) {
        // ...
    }

    // Don't
    function foo(bar) {
        const baz = Array.prototype.slice.call(arguments, foo.length);
        // ...
    }
    ```

- Use arrow function for anonymous function.

    ```ts
    // Do
    const foo = bar.filter(b => b.baz === 42);

    // Don't
    const foo = bar.filter(function(b) { return b.baz === 42; });
    ```

- Use `void` rather than `any` for return type of callbacks if the return value is ignored

    ```ts
    // Do
    function foo(bar: () => void) {
        bar();
    }

    // Don't
    function foo(bar: () => any) {
        bar();
    }
    ```

- Write more specific overloads first.

    ```ts
    // Do
    function foo(bar: string): number { }
    function foo(bar: any): object { }

    // Don't
    function foo(bar: any): number { }
    function foo(bar: string): object { }
    ```

- Prefer optional parameters to overloads.

    ```ts
    // Do
    function foo(bar: string, baz?: string): void { }

    // Don't
    function foo(bar: string): void { }
    function foo(bar: string, baz: string): void { }
    ```

- Prefer unions to overloads.

    ```ts
    // Do
    function foo(bar: string|number): number { }

    // Don't
    function foo(bar: string): number { }
    function foo(bar: number): number { }
    ```