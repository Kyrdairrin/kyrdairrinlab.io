---
layout: post
title: Starting Project Euler (and answer to problem 1)
date: 2018-02-24 01:06 +0100
categories:
  - Programming
  - Project Euler
tags:
  - C#
  - Code
comments: true
---

## Introduction

I recently found a website called [Project Euler](https://projecteuler.net/about) that offers a collection of simple to complex problems to solve with code.

I've decided to solve some of them and I'll start my blog with my solution to the first problem.

All my solutions can be found here: <https://gitlab.com/Kyrdairrin/project-euler-solutions>. They give the correct answer but may not be optimal.

## My solution

The [first problem](https://projecteuler.net/problem=1) is very easy and it took me 5 minutes to write the solution. I guess it wont always be like that 😄

```csharp
public class Problem0001
{
    public object Solve()
    {
        int max3 = MaxMultBelow(3, 1000);
        int max5 = MaxMultBelow(5, 1000);
        int max15 = MaxMultBelow(15, 1000);

        int sum3 = 3 * SumIdentity(max3);
        int sum5 = 5 * SumIdentity(max5);
        int sum15 = 15 * SumIdentity(max15);

        return sum3 + sum5 - sum15;
    }

    /// <summary>
    /// Gets the max mutiple of n below max.
    /// </summary>
    private static int MaxMultBelow(int n, int max) => (max - 1) / n;

    private static int SumIdentity(int n) => n * (n + 1) / 2;
}
```
