---
layout: post
title: Practice Python with Project Euler
date: 2018-04-05 14:13 +0100
categories:
  - Programming
  - Project Euler
tags:
  - Python
  - Code
comments: true
---

## Introduction

I started to learn Python earlier this week. I solved the first 5 problems from Project Euler as an exercise and found Python to be an enjoyable language for this kind of short problems.

I'm a beginner with Python, so my code may not be "pythonic" but I've done my best.

One thing I really like is list comprehension, to write things like:

```python
[(x, y) for x in range(1, 6) for y in range(3, 6)]
```

Or the slice notation, to get a reversed list in a nice and clear way:

```python
reversed_list = list[::-1]
```

*Note that `reversed(list)` would return an iterator on the original list and not a new list.*

## My solutions

### solutions.py

```python
import functions as f
from itertools import takewhile


def solution001():
    return sum(x for x in range(1000) if (x % 3 == 0 or x % 5 == 0))


def solution002():
    return sum([x for x in takewhile(lambda x: x <= 4e6, f.fibonacci()) if (x % 2 == 0)])


def solution003():
    return f.largest_prime_factor(600851475143)


def solution004():
    return max(i * j
                for i in range(100, 1000)
                for j in range(100, i)
                if f.is_palindrome(i * j))


def solution005():
    return f.lcm(range(1, 21))


def verify():
    ans1 = solution001()
    if ans1 != 233168:
        raise ValueError(f'Solution 001 should be 233168 (found {ans1})')

    ans2 = solution002()
    if ans2 != 4613732:
        raise ValueError(f'Solution 002 should be 4613732 (found {ans2})')

    ans3 = solution003()
    if ans3 != 6857:
        raise ValueError(f'Solution 003 should be 6857 (found {ans3})')

    ans4 = solution004()
    if ans4 != 906609:
        raise ValueError(f'Solution 004 should be 906609 (found {ans4})')

    ans5 = solution005()
    if ans5 != 232792560:
        raise ValueError(f'Solution 005 should be 232792560 (found {ans5})')


if __name__ == "__main__":
    verify()
```

## functions.py

```python
from fractions import gcd


def fibonacci():
    yield 1
    yield 1

    a, b = 1, 1

    while True:
        yield a + b
        a, b = b, a + b


def largest_prime_factor(num):
    d = 2

    while d * d <= num:
        if num % d != 0:
            d += 1
        else:
            num /= d

    return num


def is_palindrome(n):
    return str(n) == str(n)[::-1]


def lcm(numbers):
    a = numbers[0]
    b = numbers[1]
    rest = numbers[2:]

    ab_lcm = a * b // gcd(a, b)

    if(len(rest) == 0):
        return ab_lcm

    return lcm([ab_lcm, *rest])
```

### functions.tests.py

A few unit tests on my functions.

```python
import unittest
import functions as f
import itertools as it


class FunctionsTests(unittest.TestCase):

    def test_fibonacci(self):
        seq = list(it.islice(f.fibonacci(), 10))
        self.assertEqual(seq, [1, 1, 2, 3, 5, 8, 13, 21, 34, 55])


    def test_largest_prime_factor(self):
        cases = [(42, 7), (762562, 8867), (56416541, 100207), (600851475143, 6857)]
        for num, expected in cases:
            with self.subTest():
                self.assertEqual(f.largest_prime_factor(num), expected)


    def test_is_palindrome(self):
        truethy = [0, 1, 2, 3, 4, 5, 33, 77, 99, 101, 161, 202, 272, 745547, 789424987]
        falsy = [12, 34, 890, 1233210, 993299]
        for num in truethy:
            with self.subTest():
                self.assertTrue(f.is_palindrome(num))
        for num in falsy:
            with self.subTest():
                self.assertFalse(f.is_palindrome(num))


    def test_lcm(self):
        cases = [([74, 28], 1036), ([2, 40, 100, 97, 23], 446200), ([88, 43, 24, 50, 94], 13338600), ([96, 81, 81, 34, 83, 7, 96, 27], 25601184)]
        for numbers, expected in cases:
            with self.subTest():
                self.assertEqual(f.lcm(numbers), expected)


if __name__ == '__main__':
    unittest.main()
```

## Last word

I'm still in the process of learning Python but I like it already. I have the feeling that one can quickly write working code in a concise and clean way.

I still don't like the fact that there is no `private`, `protected`, etc. modifiers and that variables can be used without being declared, but maybe it's just a metter of getting used to it.