---
layout: page
title: About
permalink: /about/
---

<img align="left" class="avatar" src="/assets/img/kyrdairrin.png">

Hi, I'm Kyrdairrin. I'm a junior Software Engineer living in Paris, France. This is my first technical blog and I don't know yet what it will host. Hopefully interresting stuff :)

I have been working for a few years now at a software company, mainly with C#, JavaScript/TypeScript (React) and PowerShell. I plan to learn (or relearn) in a near future some other languages such as Ruby, Python and Haskell. I'll probably share my progress on this blog.

Apart from software development, I'm a big fan of science-fiction and fantasy (both reading and writing), video games, board games and tabletop role-playing games. I'm currently taking "night classes" to learn creative writing, board game design and Japanese language. If you know French, you'll soon be able to read about it on my other blog: <https://octachore.github.io>

You can find my open source code on [GitLab](https://gitlab.com/Kyrdairrin)